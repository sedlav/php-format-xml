<?php

if (empty($argv[1])) {
    echo "El fichero a formatear es obligatorio\n";
    exit(1);
}

$file_path = $argv[1];

if (!is_readable($file_path)) {
    echo "El fichero $file_path no existe o no se puede leer\n";
    exit(2);
}

if (empty($content = file_get_contents($file_path))) {
    echo "El contenido del fichero $file_path es vacío\n";
    exit(3);
} 

$xml = new DOMDocument();

$xml->loadXML(html_entity_decode($content));
$xml->preserveWhiteSpace= false;
$xml->formatOutput = true;

$xml_formatted = $xml->saveXML();

echo $xml->saveXML();

//file_put_contents('formatted.xml', $xml_formatted);

exit(0);
